# -*- coding: utf-8 -*-

from email.policy import default
from re import search
from odoo import models, fields, api
from odoo.exceptions import UserError


class RegisterVehicle(models.Model):
    _name = 'challenge.register.vehicle'
    _rec_name = 'settings_vehicle_id'

    settings_vehicle_id = fields.Many2one(
        "challenge.settings.vehicle",
        ondelete='cascade',
        string='Brand vehicle',
        required=True
    )

    model = fields.Char(
        string="Name model vehicle",
        # related='settings_vehicle_id.model_name',
        required=True
    )

    optionColor = [
        ("black", "Black"),
        ("red", "Red"),
        ("blue", "Blue"),
        ('orange', 'Orange')
    ]
    color = fields.Selection(
        optionColor,
        string="Color vehicle",
        required=True
    )

    date_of_purchase = fields.Date(
        string='Date of purchase',
        readonly=False,
        required=True
    )

    mileage = fields.Integer(
        string="Vehicle mileage",
        required=True
    )

    actual_price = fields.Integer(
        related='settings_vehicle_id.actual_price',
        computed='_onchange_check_fields'
    )

    number_of_annual_services = fields.Integer(
        'Number annual of services', required=True)

    services_available = fields.Integer(
        'Number of services available',
        readonly=True,
        default=2
    )

    @api.onchange('settings_vehicle_id', 'mileage', 'number_of_annual_services')
    def _onchange_check_fields(self):
        self.model = self.settings_vehicle_id.model_name
        if self.mileage >= 10000:
            value = self.actual_price * (5 / 100)
            self.actual_price = self.actual_price - value

        if self.number_of_annual_services == 1:
            self.services_available = 1
        if self.number_of_annual_services == 2:
            self.services_available = 0
        if self.number_of_annual_services == 0:
            self.services_available = 2

        if self.number_of_annual_services > 2:
            self.services_available = 2
            raise UserError('Annual number of exhausted services')

    @api.constrains('number_of_annual_services', 'mileage')
    def _check_validate_fields(self):
        if self.mileage < 0:
            raise UserError('Mileage cannot be in negative values')
        if not int(self.number_of_annual_services):
            raise UserError(
                'You cannot enter values other than number in "Number of services"'
            )
