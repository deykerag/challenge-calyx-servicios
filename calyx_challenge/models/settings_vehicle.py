# -*- coding: utf-8 -*-
from odoo import models, fields, api


class SettingsVehicle(models.Model):
    _name = 'challenge.settings.vehicle'
    _description = 'Model to register the brand of vehicles'
    _rec_name = 'name_brand'

    name_brand = fields.Char(
        string="Name brand vehicle",
        required=True
    )

    model_name = fields.Char(string='Name model vehicle')

    actual_price = fields.Integer(
        'Actual price in dolars'
    )


