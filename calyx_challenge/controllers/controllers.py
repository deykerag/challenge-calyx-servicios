# -*- coding: utf-8 -*-
from odoo import http

# class CalyxChallege(http.Controller):
#     @http.route('/calyx_challege/calyx_challege/', auth='public')
#     def index(self, **kw):
#         return "Hello, world"

#     @http.route('/calyx_challege/calyx_challege/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('calyx_challege.listing', {
#             'root': '/calyx_challege/calyx_challege',
#             'objects': http.request.env['calyx_challege.calyx_challege'].search([]),
#         })

#     @http.route('/calyx_challege/calyx_challege/objects/<model("calyx_challege.calyx_challege"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('calyx_challege.object', {
#             'object': obj
#         })